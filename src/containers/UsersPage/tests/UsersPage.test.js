import React from 'react';
import { render, screen } from '../../../utils/test-wrapper';
import { UsersPage } from '../UsersPage';
import { labels } from '../../../i18n/source';
import { usePagaCoinApi } from '../../../hooks';


jest.mock('../../../hooks/usePagaCoinApi', () => ({
    usePagaCoinApi: jest.fn(),
    API_ENDPOINT: {
        GET_USERS: 'test-user/'
    }
}));

beforeEach(() => {
    jest.resetAllMocks();
});

describe('UsersPage', () => {

    it('will render the <TableSkeleton /> component when usePagaCoinApi isLoading', () => {
        const totalUsers = 0;
        usePagaCoinApi.mockImplementation(() => ({
            reqState: {
                isLoading: true,
                data: {
                    totalElements: totalUsers
                }
            },
            setGetRequestPath: jest.fn()
        }));
        render(<UsersPage />);
        expect(screen.getByRole('heading')).toHaveTextContent(labels.appTotalUsers(totalUsers));
        expect(screen.getByTestId('TableSkeleton')).toBeInTheDocument();
    });
    it('will render the <TableSkeleton /> component when usePagaCoinApi isLoading', () => {
        const totalUsers = 0;
        usePagaCoinApi.mockImplementation(() => ({
            reqState: {
                isLoading: true,
                data: {
                    totalElements: totalUsers
                }
            },
            setGetRequestPath: jest.fn()
        }));
        render(<UsersPage />);
        expect(screen.getByRole('heading')).toHaveTextContent(labels.appTotalUsers(totalUsers));
        expect(screen.getByTestId('TableSkeleton')).toBeInTheDocument();
    });

    it('will render the <UsersTable /> component when usePagaCoinApi resolve', () => {
        const totalUsers = 1;
        usePagaCoinApi.mockImplementation(() => ({
            reqState: {
                isLoading: false,
                data: {
                    totalElements: totalUsers,
                    content: []
                }
            },
            setGetRequestPath: jest.fn()
        }));
        render(<UsersPage />);
        expect(screen.getByRole('heading')).toHaveTextContent(labels.appTotalUsers(totalUsers));
        expect(screen.getByTestId('UsersTable')).toBeInTheDocument();
    });

});
