import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import { UsersTable } from '../../components/UsersTable/UsersTable';
import { usePagaCoinApi, API_ENDPOINT } from '../../hooks';
import { usersTableConfig } from '../../components/UsersTable/usersTableConfig';
import { TableSkeleton } from '../../components/TableSkeleton';


const { GET_USERS } = API_ENDPOINT;

const useStyles = makeStyles((theme) => ({
    infoContainer: {
        height: 300,
        backgroundColor: theme.palette.primary.main,
        '& h2': {
            margin: 0,
            textAlign: 'center',
            paddingTop: 70
        },
        [theme.breakpoints.up('lg')]: {
            maxWidth: 'unset',
        },
    },
    tableContainer: {
        marginTop: -150
    }
}));

export const UsersPage = () => {

    const classes = useStyles();
    const location = useLocation();
    const endpointSearch = GET_USERS + location.search;
    const { reqState, setGetRequestPath } = usePagaCoinApi(endpointSearch, {
        content: [],
        totalElements: 0
    });

    useEffect(() => {
        setGetRequestPath(endpointSearch);
    }, [endpointSearch, location.search, setGetRequestPath]);

    const renderUsersTable = () => {
        if (reqState.isLoading) {
            return (
                <TableSkeleton rowsNumber={reqState.data.size || 4}
                               cellsNumber={6} />
            );
        }
        return (
            <UsersTable rows={reqState.data.content}
                        config={usersTableConfig}
                        rowsPerPage={reqState.data.size}
                        page={reqState.data.number}
                        count={reqState.data.totalElements} />
        );
    };

    return (
        <div data-testid="UsersPage">
            <Container className={classes.infoContainer}>
                <h2>{`Total Users found: ${reqState.data.totalElements}`}</h2>
            </Container>
            <Container className={classes.tableContainer}>
                {renderUsersTable()}
            </Container>
        </div>
    );
};
