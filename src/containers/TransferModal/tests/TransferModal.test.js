import React from 'react';
import { render, screen } from '../../../utils/test-wrapper';
import { TransferModal } from '../TransferModal';
import { usePagaCoinApi } from '../../../hooks';
import { StateContext } from '../../../contexts/AppStore/AppStore';


jest.mock('../../../hooks/usePagaCoinApi', () => ({
    usePagaCoinApi: jest.fn(),
    API_ENDPOINT: {
        POST_WALLET_TRANSFER: 'test-user/'
    }
}));

beforeEach(() => {
    jest.resetAllMocks();
});

describe('TransferModal', () => {

    const stateMock = {
        showTransferModal: false,
        restWallets: [],
        currentWallet: {
            hash: 'test-hash'
        }
    };

    it('wont render any comp if showTransferModal is falsy', () => {
        usePagaCoinApi.mockImplementation(() => ({
            reqState: {
                isLoading: false,
                isError: false
            }
        }));
        render(
            <StateContext.Provider value={stateMock}>
                <TransferModal />
            </StateContext.Provider>
        );
        expect(screen.queryByRole('presentation')).not.toBeInTheDocument();
    });

    it('will render a TransferForm comp if data and isError are falsy', () => {
        stateMock.showTransferModal = true;
        usePagaCoinApi.mockImplementation(() => ({
            reqState: {
                isLoading: false,
                isError: false
            }
        }));
        render(
            <StateContext.Provider value={stateMock}>
                <TransferModal />
            </StateContext.Provider>
        );
        expect(screen.getByRole('presentation')).toBeInTheDocument();
        expect(screen.getByTestId('transfer-form')).toBeInTheDocument();
    });

});
