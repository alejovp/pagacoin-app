import React, { useCallback, useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core/styles';
import { DispatchContext, StateContext } from '../../contexts/AppStore/AppStore';
import { setCurrentWallets, setShowTranferModal } from '../../contexts/AppStore/actions';
import { TransferForm } from '../../components/TransferForm';
import { TransferResult } from '../../components/TransferResult';
import { API_ENDPOINT, usePagaCoinApi } from '../../hooks';
import { labels } from '../../i18n/source';


const { POST_WALLET_TRANSFER } = API_ENDPOINT;

const useStyles = makeStyles({
    close: {
        position: 'absolute',
        right: 0
    }
});

export const TransferModal = () => {

    const classes = useStyles();
    const history = useHistory();
    const { showTransferModal } = useContext(StateContext);
    const dispatch = useContext(DispatchContext);
    const [transferInfo, setTransferInfo] = useState({});
    const { reqState, setPostRequestParams, resetReqState } = usePagaCoinApi();
    const { data: success, isLoading, isError } = reqState;

    const handleClose = useCallback(() => {
        dispatch(setShowTranferModal(false));
        dispatch(setCurrentWallets({}, []));
        setTransferInfo({});
        if (success) {
            history.go(0);
        }
    }, [dispatch, history, success]);

    const handleSubmit = useCallback((params) => {
        setTransferInfo(params);
        setPostRequestParams({
            path: POST_WALLET_TRANSFER,
            params
        });
    }, [setPostRequestParams]);

    const handleOnExited = useCallback(() => {
        resetReqState();
    }, [resetReqState]);

    const renderModalContent = () => {
        if (success || isError) {
            return (
                <TransferResult transferInfo={transferInfo}
                                error={isError}
                                onClick={handleClose} />
            );
        }
        return (
            <TransferForm onCancel={handleClose}
                          onSubmit={handleSubmit}
                          loading={isLoading}
            />
        );
    };

    return (
        <Dialog open={showTransferModal}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
                onExited={handleOnExited}>
            <IconButton aria-label="close transfer modal"
                        className={classes.close}
                        onClick={handleClose}>
                <CloseIcon />
            </IconButton>
            <DialogTitle id="form-dialog-title">
                {labels.appTransfer}
            </DialogTitle>
            <DialogContent>
                {renderModalContent()}
            </DialogContent>
        </Dialog>
    );
};
