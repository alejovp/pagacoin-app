export const REQUEST_INIT = 'pagacoin-app/hooks/REQUEST_INIT';
export const REQUEST_SUCCESS = 'pagacoin-app/hooks/REQUEST_SUCCESS';
export const REQUEST_FAILURE = 'pagacoin-app/hooks/REQUEST_FAILURE';
export const RESET_REQ_STATE = 'pagacoin-app/hooks/RESET_REQ_STATE';
