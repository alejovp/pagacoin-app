import axios from 'axios';
import { renderHook, act } from '@testing-library/react-hooks';
import { usePagaCoinApi, API_PREFIX } from '../usePagaCoinApi';


const testUrl = 'test-path';
const testUrlSearch = `${testUrl}?page=4`;
const testParams = {
    path: testUrl,
    params: {
        hash: 'test-hash'
    }
};

beforeEach(() => {
    jest.resetAllMocks();
});

describe('usePagaCoinApi', () => {

    describe('GET requests', () => {

        it('will do a get request when comp mounts and pass the req state', async () => {
            const mockHits = ['user1', 'user2'];
            const promise = Promise.resolve({
                data: {
                    hits: mockHits
                }
            });

            axios.get.mockImplementationOnce(() => promise);
            const { result } = renderHook(() => usePagaCoinApi(testUrl, { hits: [] }));

            expect(result.current.reqState).toEqual({
                isLoading: true,
                data: {
                    hits: []
                },
                isError: false
            });

            await act(() => promise);

            expect(result.current.reqState).toEqual({
                isLoading: false,
                data: {
                    hits: mockHits
                },
                isError: false
            });
        });

        it('Will do a new get request when comp set a new path', async () => {
            const promise = Promise.resolve({
                data: {}
            });

            axios.get.mockImplementationOnce(() => promise);
            const { result } = renderHook(() => usePagaCoinApi(testUrl, { hits: [] }));

            act(() => result.current.setGetRequestPath(testUrlSearch));
            await act(() => promise);

            expect(axios.get).toHaveBeenNthCalledWith(1, `${API_PREFIX}/${testUrl}`);
            expect(axios.get).toHaveBeenNthCalledWith(2, `${API_PREFIX}/${testUrlSearch}`);
        });
    });

    describe('POSTS requests', () => {

        const testRes = {
            message: 'ok'
        };
        const promise = Promise.resolve({
            data: testRes
        });

        beforeEach(() => {
            axios.post.mockImplementationOnce(() => promise);
        });

        it('Will do a post request when comp set new postRequestParams', async () => {
            const { result } = renderHook(() => usePagaCoinApi());

            expect(result.current.reqState).toEqual({ isError: false, isLoading: false });

            act(() => result.current.setPostRequestParams(testParams));
            await act(() => promise);

            expect(axios.post).toHaveBeenCalledWith(`${API_PREFIX}/${testUrl}`, testParams.params);
            expect(result.current.reqState).toEqual({ isError: false, isLoading: false, data: testRes });
        });

        it('Will do a request state reset through the resetReqState invoke', async () => {
            const { result } = renderHook(() => usePagaCoinApi());

            act(() => result.current.setPostRequestParams(testParams));
            await act(() => promise);

            expect(result.current.reqState).toEqual({ isError: false, isLoading: false, data: testRes });

            act(() => result.current.resetReqState());

            expect(result.current.reqState).toEqual({ isError: false, isLoading: false });
        });

    });

});
