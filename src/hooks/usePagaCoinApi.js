import { useEffect, useReducer, useState } from 'react';
import axios from 'axios';
import {
    REQUEST_FAILURE,
    REQUEST_INIT,
    REQUEST_SUCCESS,
    RESET_REQ_STATE
} from './constants';


const dataReducer = (state, action) => {
    switch (action.type) {
        case REQUEST_INIT:
            return {
                ...state,
                isLoading: true,
                isError: false
            };
        case REQUEST_SUCCESS:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
            };
        case REQUEST_FAILURE:
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        default:
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: undefined
            };
    }
};

export const API_PREFIX = 'http://localhost:8080/pagacoin/api'; // move to .env or config file
export const API_ENDPOINT = {
    GET_USERS: 'user/',
    POST_WALLET_TRANSFER: 'wallet/transfer'
};

export const usePagaCoinApi = (initialPath, initialData) => {
    const [getRequestPath, setGetRequestPath] = useState(initialPath);
    const [postRequestParams, setPostRequestParams] = useState({ path: '', params: {} });

    const [reqState, dispatch] = useReducer(dataReducer, {
        isLoading: false,
        isError: false,
        data: initialData,
    });

    const resetReqState = () => dispatch({ type: RESET_REQ_STATE });

    useEffect(() => {
        let didCancel = false;

        const doRequest = async () => {
            dispatch({ type: REQUEST_INIT });

            try {
                let result;
                if (postRequestParams.path) {
                    const { path, params } = postRequestParams;
                    result = await axios.post(`${API_PREFIX}/${path}`, params);
                } else {
                    result = await axios.get(`${API_PREFIX}/${getRequestPath}`);
                }

                if (!didCancel) {
                    dispatch({ type: REQUEST_SUCCESS, payload: result.data });
                }
            } catch (error) {
                if (!didCancel) {
                    dispatch({ type: REQUEST_FAILURE });
                }
            }
        };

        if (getRequestPath || postRequestParams.path) {
            doRequest();
        }

        return () => {
            didCancel = true;
        };
    }, [getRequestPath, postRequestParams]);

    return {
        reqState,
        setGetRequestPath,
        setPostRequestParams,
        resetReqState
    };
};
