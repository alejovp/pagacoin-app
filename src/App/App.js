import React from 'react';
import {
    BrowserRouter as Router
} from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Header } from '../components/Header';
import { lightTheme } from '../styles/themes/lightTheme';
import { AppStore } from '../contexts/AppStore/AppStore';
import { TransferModal } from '../containers/TransferModal';
import { Main } from '../components/Main';


export const App = () => {
    return (
        <AppStore>
            <MuiThemeProvider theme={lightTheme}>
                <Router>
                    <CssBaseline />
                    <Header />
                    <Main />
                    <TransferModal />
                </Router>
            </MuiThemeProvider>
        </AppStore>
    );
};
