import React from 'react';
import { render, screen, act } from '../../utils/test-wrapper';
import { App } from '../App';
import { labels } from '../../i18n/source';
import { ROUTES } from  '../../router/routes';


describe('App', () => {

    test('should render the Home comp for a matching / path', () => {
        render(<App />);
        expect(screen.getByTestId('Header')).toBeInTheDocument();
        expect(screen.getByText(labels.appWelcomeMsg)).toBeInTheDocument();
    });

    test('should render the UsersPage comp for the users path', async () => {
        const promise = Promise.resolve({ data: {} });
        render(<App />, { route: ROUTES.USERS.path });
        await act(() => promise);
        expect(screen.getByTestId('UsersPage')).toBeInTheDocument();
    });

});
