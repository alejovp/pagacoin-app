import {
    SET_CURRENT_WALLETS,
    SET_OPEN_DRAWER,
    SET_SHOW_TRANSFER_MODAL
} from './constants';


export const setShowTranferModal = (value) => ({
    type: SET_SHOW_TRANSFER_MODAL,
    payload: value
});

export const setCurrentWallets = (wallet, wallets) => ({
    type: SET_CURRENT_WALLETS,
    payload: {
        wallet,
        wallets
    }
});

export const setOpenDrawer = (value) => ({
    type: SET_OPEN_DRAWER,
    payload: value
});
