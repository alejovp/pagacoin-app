import { SET_CURRENT_WALLETS, SET_OPEN_DRAWER, SET_SHOW_TRANSFER_MODAL } from './constants';


export const appReducer = (state, action) => {
    switch (action.type) {
        case SET_SHOW_TRANSFER_MODAL:
            return {
                ...state,
                currentWallet: {
                    ...state.currentWallet
                },
                restWallets: state.restWallets.slice(),
                showTransferModal: action.payload
            };
        case SET_OPEN_DRAWER:
            return {
                ...state,
                currentWallet: {
                    ...state.currentWallet
                },
                restWallets: state.restWallets.slice(),
                openDrawer: action.payload
            };
        case SET_CURRENT_WALLETS:
            return {
                ...state,
                currentWallet: action.payload.wallet,
                restWallets: action.payload.wallets
            };
        default:
            return {
                ...state,
                currentWallet: {
                    ...state.currentWallet
                },
                restWallets: state.restWallets.slice()
            };
    }
};
