export const labels = {
    appTitle: 'PagaCoin BO',
    appLinkHome: 'Dashboard',
    appLinkUsers: 'Users',
    appWelcomeMsgTitle: 'Welcome to PagaCoin Back Office App',
    appWelcomeMsg: 'Hello super admin, in the Users section you will find all our users and his wallets.',
    appTotalUsers: (total) => `Total Users found: ${total}`,
    appOk: 'Ok',
    appTransfer: 'Transfer',
    appTransferFormDestinyWallet: 'Please select a destiny wallet, then click on submit button to do the Pagacoins transfer.',
    appTransferRadiosLabel: 'To one of my Wallets or a different one?',
    appTransferFormAmount: 'Amount',
    appTranferFormRadioMine: 'Mine',
    appTranferFormRadioOther: 'Other',
    appSubmit: 'Submit',
    appTranferResultSuccess: (hashTo) => `Transfer to wallet ${hashTo} have been processed successfully!.`,
    appTranferResultSuccessWallet: (hashFrom, balance, amount) => `Your current balance in wallet ${hashFrom} is ${balance - amount} pagacoins.`,
    appTranferResultError: 'Ups something went wrong, plesase check your wallet details and try again!.'
};
