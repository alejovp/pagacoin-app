import { Home } from '../components/Home';
import { UsersPage } from '../containers/UsersPage';


export const ROUTES = {
    HOME: {
        path: '/',
        name: 'Home',
        component: Home
    },
    USERS: {
        path: '/users',
        name: 'Users',
        component: UsersPage
    }
};
