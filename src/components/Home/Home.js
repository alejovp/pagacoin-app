import React from 'react';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { labels } from '../../i18n/source';


const useStyles = makeStyles((theme) => ({
    container: {
        height: 300,
        backgroundColor: theme.palette.primary.main,
        paddingTop: 150,
        [theme.breakpoints.up('lg')]: {
            maxWidth: 'unset',
        },
    },
    paper: {
        minHeight: 300,
        padding: 30,
        textAlign: 'center'
    },
    tableContainer: {
        marginTop: -150
    }
}));

export const Home = () => {

    const  classes = useStyles();

    return (
        <Container className={classes.container}>
            <Paper className={classes.paper}>
                <Typography variant="h3"
                            component="h2"
                            gutterBottom>
                    {labels.appWelcomeMsgTitle}
                </Typography>
                <Typography variant="body1"
                            gutterBottom>
                    {labels.appWelcomeMsg}
                </Typography>
            </Paper>
        </Container>
    );
};
