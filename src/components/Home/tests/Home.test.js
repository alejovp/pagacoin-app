import React from 'react';
import { render, screen } from '../../../utils/test-wrapper';
import { Home } from '../Home';
import { labels } from '../../../i18n/source';

describe('Home', () => {

    it('will render the <Home /> component with app welcome message', () => {
        render(<Home />);
        expect(screen.getByRole('heading')).toHaveTextContent(labels.appWelcomeMsgTitle);
    });

});
