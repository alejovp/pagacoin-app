import React from 'react';
import { render, screen } from '../../../utils/test-wrapper';
import { Header } from '../Header';
import { labels } from '../../../i18n/source';

describe('Header', () => {

    it('will render the <Header /> component with app title', () => {
        render(<Header />);
        expect(screen.getByRole('heading')).toHaveTextContent(labels.appTitle);
    });

});
