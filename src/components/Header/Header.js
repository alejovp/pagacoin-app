import React, { useCallback, useContext } from 'react';
import clsx from 'clsx';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { labels } from '../../i18n/source';
import { DispatchContext, StateContext } from '../../contexts/AppStore/AppStore';
import { setOpenDrawer } from '../../contexts/AppStore/actions';


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: theme.constants.drawerWidth,
        width: `calc(100% - ${theme.constants.drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    link: {
        color: theme.palette.primary.contrastText,
        textDecoration: 'none',
        '&:hover': {
            textDecoration: 'none'
        }
    },
    divider: {
        margin: '5px 20px'
    }
}));

export const Header = () => {
    const classes = useStyles();
    const { openDrawer } = useContext(StateContext);
    const dispatch = useContext(DispatchContext);

    const handleDrawerOpen = useCallback(() => (
        dispatch(setOpenDrawer(true))
    ), [dispatch]);

    return (
        <AppBar
            data-testid="Header"
            position="fixed"
            className={clsx(classes.appBar, {
                [classes.appBarShift]: openDrawer,
            })}
        >
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    className={clsx(classes.menuButton, {
                        [classes.hide]: openDrawer,
                    })}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6"
                            noWrap>
                    {labels.appTitle}
                </Typography>
            </Toolbar>
        </AppBar>
    );
};
