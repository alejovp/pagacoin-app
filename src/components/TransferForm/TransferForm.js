import React, {
    useCallback,
    useContext,
    useEffect,
    useState
} from 'react';
import PropTypes from 'prop-types';
import DialogContentText from '@material-ui/core/DialogContentText';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Divider from '@material-ui/core/Divider';
import SendIcon from '@material-ui/icons/Send';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import { StateContext } from '../../contexts/AppStore/AppStore';
import { ToInput } from '../ToInput';
import { labels } from '../../i18n/source';


const useStyles = makeStyles(theme => ({
    grid: {
        textAlign: 'center'
    },
    form: {
        textAlign: 'center'
    },
    fieldset: {
        alignItems: 'center'
    },
    container: {
        padding: theme.spacing(3),
        textAlign: 'center'
    }
}));

const initFormState = {
    hashFrom: '',
    hashTo: '',
    amount: 0
};

export const TransferForm = ({ onSubmit, loading }) => {

    const classes = useStyles();
    const { currentWallet, restWallets } = useContext(StateContext);
    const [formState, setFormState] = useState(initFormState);
    const [radioValue, setRadioValue] = useState('input');
    const { hashFrom, hashTo, amount } = formState;
    const isValidForm = !!(hashFrom && hashTo && amount);

    const handleRadioChange = useCallback((event) => {
        setRadioValue(event.target.value);
    }, [setRadioValue]);

    const onChangeHandler = useCallback(({ target }) => {
        const { name, value } = target;
        setFormState(prevState => ({
            ...prevState,
            [name]: value
        }));
    }, [setFormState]);

    const onSubmitHandler = useCallback(
        () => onSubmit(formState),
        [onSubmit, formState]
    );

    useEffect(() => {
        const target = {
            name: 'hashFrom',
            value: currentWallet.hash
        };
        onChangeHandler({ target });
    }, [currentWallet, onChangeHandler]);

    const renderFromInputRadios = () => {
        if (restWallets.length === 0) {
            return null;
        }
        return (
            <FormControl component="fieldset" className={classes.fieldset}>
                <FormLabel component="legend">
                    {labels.appTransferRadiosLabel}
                </FormLabel>
                <RadioGroup aria-label="destinyWallets"
                            name="destinyWallets"
                            value={radioValue}
                            onChange={handleRadioChange}
                            row>
                    <FormControlLabel value="select"
                                      control={<Radio />}
                                      label={labels.appTranferFormRadioMine} />
                    <FormControlLabel value="input"
                                      control={<Radio />}
                                      label={labels.appTranferFormRadioOther} />
                </RadioGroup>
            </FormControl>
        );
    };

    const renderButtonEndIcon = () => (
        loading ? <CircularProgress size={24} /> : <SendIcon />
    );

    return (
        <>
            <DialogContentText>
                {labels.appTransferFormDestinyWallet}
            </DialogContentText>
            <form noValidate
                  autoComplete="off"
                  data-testid="transfer-form"
                  className={classes.form}>
                {renderFromInputRadios()}
                <Container className={classes.container}>
                    <Grid container
                          spacing={1}>
                        <Grid item xs={12}
                              sm={6}
                              className={classes.grid}>
                            <ToInput type={radioValue}
                                     onChange={onChangeHandler}
                                     options={restWallets} />
                        </Grid>
                        <Grid item xs={12}
                              sm={6}
                              className={classes.grid}>
                            <TextField
                            type="number"
                            variant="outlined"
                            inputProps={{
                                maxLength: 10
                            }}
                            label={labels.appTransferFormAmount}
                            name="amount"
                            onChange={onChangeHandler}
                            required />
                        </Grid>
                    </Grid>
                </Container>
                <Divider />
                <Container className={classes.container}>
                    <Button onClick={onSubmitHandler}
                            size="large"
                            variant="contained"
                            color="primary"
                            disableElevation
                            disabled={!isValidForm || loading}
                            endIcon={renderButtonEndIcon()}>
                        {labels.appSubmit}
                    </Button>
                </Container>
            </form>
        </>
    );
};

TransferForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    loading: PropTypes.bool
};
