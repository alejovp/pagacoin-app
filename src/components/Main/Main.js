import React, { useCallback, useContext } from 'react';
import clsx from 'clsx';
import {
    Link as RouterLink,
    Switch,
    Route,
    useLocation
} from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import { makeStyles } from '@material-ui/core/styles';
import { Home } from '../Home';
import { UsersPage } from '../../containers/UsersPage';
import { ROUTES } from '../../router/routes';
import { DispatchContext, StateContext } from '../../contexts/AppStore/AppStore';
import { setOpenDrawer } from '../../contexts/AppStore/actions';
import { labels } from '../../i18n/source';


const useStyles = makeStyles((theme) => ({
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: theme.constants.drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: theme.constants.drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1
    },
}));

export const Main = () => {

    const classes = useStyles();
    const location = useLocation();
    const { openDrawer } = useContext(StateContext);
    const dispatch = useContext(DispatchContext);

    const handleDrawerClose = useCallback(() => (
        dispatch(setOpenDrawer(false))
    ), [dispatch]);

    const ListItemLink = (props) => (
        <ListItem button component={RouterLink} {...props} />
    );

    return (
        <>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: openDrawer,
                    [classes.drawerClose]: !openDrawer,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: openDrawer,
                        [classes.drawerClose]: !openDrawer,
                    }),
                }}
            >
                <div className={classes.toolbar}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <Divider />
                <List>
                    <ListItemLink to={ROUTES.HOME.path}
                                  selected={location.pathname === ROUTES.HOME.path}>
                        <ListItemIcon>
                            <DashboardIcon />
                        </ListItemIcon>
                        <ListItemText primary={labels.appLinkHome} />
                    </ListItemLink>
                    <ListItemLink to={ROUTES.USERS.path}
                                  selected={location.pathname === ROUTES.USERS.path}>
                        <ListItemIcon>
                            <PeopleIcon />
                        </ListItemIcon>
                        <ListItemText primary={labels.appLinkUsers} />
                    </ListItemLink>
                </List>
            </Drawer>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                <Switch>
                    <Route exact path={ROUTES.USERS.path}>
                        <UsersPage />
                    </Route>
                    <Route path={ROUTES.HOME.path}>
                        <Home />
                    </Route>
                </Switch>
            </main>
        </>
    );
};
