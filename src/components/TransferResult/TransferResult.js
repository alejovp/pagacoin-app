import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import DialogContentText from '@material-ui/core/DialogContentText';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import Fade from '@material-ui/core/Fade';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import { green, red } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import { StateContext } from '../../contexts/AppStore/AppStore';
import { labels } from '../../i18n/source';


const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginBottom: 25
    },
    iconContainer: {
        height: 70,
        width: 70,
        borderRadius: '50%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: ({ error }) => error ? red[500] : green[500],
        margin: '25px 0px'
    },
    buttonContainer: {
        paddingTop: theme.spacing(3),
        textAlign: 'center'
    },
    divider: {
        width: '100%'
    }
}));

export const TransferResult = ({
    transferInfo, error, onClick
}) => {

    const classes = useStyles({ error });
    const { currentWallet } = useContext(StateContext);
    const [fade, setFade] = useState(false);
    const { hashTo, amount, hashFrom } = transferInfo;

    useEffect(() => {
        setFade(true);
    }, [setFade]);

    const renderResultIcon = () => {
        if (error) {
            return (
                <CloseIcon fontSize="large"
                           data-testid="error-icon" />
            );
        }
        return (
            <CheckIcon fontSize="large"
                       data-testid="success-icon" />
        );
    };

    const renderResultText = () => {
        if (error) {
            return (
                <DialogContentText>
                    {labels.appTranferResultError}
                </DialogContentText>
            );
        }
        return (
            <>
                <DialogContentText>
                    {labels.appTranferResultSuccess(hashTo)}
                </DialogContentText>
                <DialogContentText>
                    {labels.appTranferResultSuccessWallet(hashFrom, currentWallet.balance, amount)}
                </DialogContentText>
            </>
        );
    };

    return (
        <div className={classes.container}>
            <Fade in={fade}>
                <div className={classes.iconContainer}>
                    {renderResultIcon()}
                </div>
            </Fade>
            {renderResultText()}
            <Divider className={classes.divider}/>
            <Container className={classes.buttonContainer}>
                <Button onClick={onClick}
                        size="large"
                        variant="contained"
                        disableElevation>
                    {labels.appOk}
                </Button>
            </Container>
        </div>
    );
};

TransferResult.propTypes = {
    transferInfo: PropTypes.shape({
        hashFrom: PropTypes.string.isRequired,
        hashTo: PropTypes.string.isRequired,
        amount: PropTypes.string.isRequired,
    }),
    error: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired
};
