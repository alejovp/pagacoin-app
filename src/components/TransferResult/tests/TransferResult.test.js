import React from 'react';
import { fireEvent, render, screen } from '../../../utils/test-wrapper';
import { TransferResult } from '../TransferResult';
import { StateContext } from '../../../contexts/AppStore/AppStore';
import { usersResponseMock } from '../../../../__mocks__/usersResponseMock';
import { labels } from '../../../i18n/source';


const mockWallet = usersResponseMock.content[0].wallets[0];

const mockTransferInfo = {
    hashFrom: 'hash-from',
    hashTo: 'hash-to',
    amount: '1000'
};

beforeEach(() => {
    jest.resetAllMocks();
});

describe('TransferResult', () => {

    const onClickMock = jest.fn();

    it('will render a succes icon with resume tranfer info', () => {
        render(
            <StateContext.Provider value={{ currentWallet: mockWallet }}>
                <TransferResult transferInfo={mockTransferInfo}
                                onClick={onClickMock}
                                error={false} />
            </StateContext.Provider>
        );

        const { hashFrom, hashTo, amount } = mockTransferInfo;
        const succesIcon = screen.getByTestId('success-icon');
        const transferResult = screen.getByText(labels.appTranferResultSuccess(hashTo));
        const currentBalance = screen.getByText(labels.appTranferResultSuccessWallet(hashFrom, mockWallet.balance, amount));

        expect(succesIcon).toBeInTheDocument();
        expect(transferResult).toBeInTheDocument();
        expect(currentBalance).toBeInTheDocument();
    });

    it('will render a succes icon with resume tranfer info', () => {
        render(
            <StateContext.Provider value={{ currentWallet: mockWallet }}>
                <TransferResult transferInfo={mockTransferInfo}
                                onClick={onClickMock}
                                error />
            </StateContext.Provider>
        );

        const errorIcon = screen.getByTestId('error-icon');
        const transferResult = screen.getByText(labels.appTranferResultError);
        expect(errorIcon).toBeInTheDocument();
        expect(transferResult).toBeInTheDocument();
    });

    it('will trigger the onClick callback prop when button is clicked', () => {
        render(
            <StateContext.Provider value={{ currentWallet: mockWallet }}>
                <TransferResult transferInfo={mockTransferInfo}
                                onClick={onClickMock}
                                error={false} />
            </StateContext.Provider>
        );
        const okButton = screen.getByRole('button');
        fireEvent.click(okButton);

        expect(onClickMock).toBeCalledTimes(1);
    });

});
