import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
        marginBottom: theme.spacing(1) / 2,
        [theme.breakpoints.down('xs')]: {
            width: 230,
        },
    },
    typography: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    hide: {
        [theme.breakpoints.down('xs')]: {
            display: 'none',
        },
    },
    iconButton: {
        padding: 10,
    },
    buttonContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    divider: {
        height: 28,
        margin: 4,
    },
}));

export const Wallet = ({ wallet, onClick }) => {

    const classes = useStyles();
    const onClickHandler = useCallback(() => (
        onClick(wallet)
    ), [onClick, wallet]);

    const renderButton = () => {
        if (onClick) {
            return (
                <IconButton color="primary"
                            className={classes.iconButton}
                            onClick={onClickHandler}
                            aria-label="transfer"
                            disabled={!wallet.balance}>
                    <SendIcon />
                </IconButton>
            );
        }
        return null;
    };

    return (
        <Paper className={classes.root}
               data-testid="Wallet">
            <Typography className={clsx(classes.typography, classes.hide)}>
                {wallet.hash}
            </Typography>
            <MonetizationOnIcon />
            <Typography className={classes.typography}>
                {wallet.balance}
            </Typography>
            <div className={classes.buttonContainer}>
                <Divider className={classes.divider} orientation="vertical" />
                {renderButton()}
            </div>
        </Paper>
    );
};

Wallet.propTypes = {
    wallet: PropTypes.shape({
        hash: PropTypes.string.isRequired,
        balance: PropTypes.number.isRequired
    }),
    onClick: PropTypes.func
};
