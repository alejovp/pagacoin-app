import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TableFooter from '@material-ui/core/TableFooter';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core';


const useStyles = makeStyles({
    cell: {
        height: 60
    },
    tableFooter: {
        height: 60
    }
});

export const TableSkeleton = ({ cellsNumber, rowsNumber }) => {

    const classes = useStyles();

    const renderRowCells = () => {
        const cells = [];
        for (let i = 0; i < cellsNumber; i++) {
            cells.push(
                <TableCell key={i}
                           className={classes.cell}
                           align="center">
                    <div>
                        <Skeleton variant="text" />
                    </div>
                </TableCell>
            );
        }
        return cells;
    };

    const renderBodyRows = () => {
        const cells = renderRowCells();
        const rows = [];
        for (let i = 0; i < rowsNumber; i++) {
            rows.push(
                <TableRow key={i}>
                    {cells}
                </TableRow>
            );
        }
        return rows;
    };

    return (
        <TableContainer component={Paper}
                        data-testid="TableSkeleton">
            <Table size="small"
                   aria-label="purchases">
                <TableHead>
                    <TableRow>
                        {renderRowCells()}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {renderBodyRows()}
                </TableBody>
                <TableFooter>
                    <TableRow className={classes.tableFooter}>
                        <TableCell colSpan={cellsNumber}>
                            <Skeleton variant="text" />
                        </TableCell>
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    );
};

TableSkeleton.propTypes = {
    cellsNumber: PropTypes.number.isRequired,
    rowsNumber: PropTypes.number.isRequired
};
