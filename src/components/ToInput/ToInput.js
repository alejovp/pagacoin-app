import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


const useStyles = makeStyles({
    formControl: {
        minWidth: 195,
    },
});

export const ToInput = ({ type, onChange, options }) => {

    const classes = useStyles();

    if (type === 'select') {
        const renderOptions = () => (
            options.map(option => (
                <MenuItem value={option.hash}
                          key={option.walletId}>
                    {option.hash}
                </MenuItem>
            ))
        );

        return (
            <FormControl variant="outlined"
                         className={classes.formControl}
                         required>
                <InputLabel id="wallets-select">To Hash</InputLabel>
                <Select
                    labelId="wallets-select"
                    defaultValue=""
                    onChange={onChange}
                    name="hashTo"
                    label="To Hash"
                >
                    {renderOptions()}
                </Select>
            </FormControl>
        );
    }
    return (
        <TextField
            label="To Hash"
            variant="outlined"
            inputProps={{
                maxLength: 10
            }}
            name="hashTo"
            onChange={onChange}
            required />
    );
};

ToInput.propTypes = {
    type: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            walletId: PropTypes.number.isRequired,
            hash: PropTypes.string.isRequired
        })
    )
};
