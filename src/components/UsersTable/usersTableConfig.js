export const usersTableConfig = {
    userCells: [
        {
            name: 'User Id',
            param: 'userId',
            align: 'center',
            hide: true
        },
        {
            name: 'User Name',
            param: 'userName',
            align: 'center'
        },
        {
            name: 'First Name',
            param: 'firstName',
            align: 'center',
            hide: true
        },
        {
            name: 'Last Name',
            param: 'lastName',
            align: 'center',
            hide: true
        },
        {
            name: 'Wallets',
            param: 'wallets',
            align: 'center',
            type: 'array',
            hide: true
        },
        {
            name: 'Total Balance',
            param: 'totalBalance',
            align: 'center',
        },
    ]
};
