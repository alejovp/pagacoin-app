import React from 'react';
import { usersResponseMock } from '../../../../__mocks__/usersResponseMock';
import { fireEvent, render, screen } from '../../../utils/test-wrapper';
import { usersTableConfig } from '../usersTableConfig';
import { UsersTableRow } from '../UsersTableRow';
import { DispatchContext } from '../../../contexts/AppStore/AppStore';
import { setCurrentWallets, setShowTranferModal } from '../../../contexts/AppStore/actions';


// a user with just one wallet
const rowMock = usersResponseMock.content[0];

jest.mock('../../../contexts/AppStore/actions', () => ({
    setCurrentWallets: jest.fn(),
    setShowTranferModal: jest.fn()
}));

beforeEach(() => {
    jest.resetAllMocks();
});

describe('UsersTableRow', () => {

    const dispatch = jest.fn();

    beforeEach(() => {
        render(
            <DispatchContext.Provider value={dispatch}>
                <table>
                    <tbody>
                        <UsersTableRow config={usersTableConfig}
                                       row={rowMock} />
                    </tbody>
                </table>
            </DispatchContext.Provider>
        );
    });

    it('will render a row with expand button and as many cells as items in config', () => {
        usersTableConfig.userCells.forEach(({ param, type }) => {
            let text = rowMock[param];
            if (type === 'array') {
                text = rowMock[param].length;
            }
            const cell = screen.getAllByText(text)[0];
            expect(cell).toBeInTheDocument();
        });
    });

    test('if expand button is clicked then will show another row with the wallets', () => {
        const expandButton = screen.getByLabelText('expand row');
        fireEvent.click(expandButton);

        const wallets = screen.getAllByTestId('Wallet');
        expect(wallets).toHaveLength(rowMock.wallets.length);
    });

    test('if transfer button is clicked the dispatch method will be called', () => {
        const expandButton = screen.getByLabelText('expand row');
        fireEvent.click(expandButton);

        const transferButton = screen.getByLabelText('transfer');
        fireEvent.click(transferButton);

        expect(dispatch).toHaveBeenNthCalledWith(1,
            setCurrentWallets(rowMock.wallets[0], []));
        expect(dispatch).toHaveBeenNthCalledWith(2,
            setShowTranferModal(true));
    });

});
