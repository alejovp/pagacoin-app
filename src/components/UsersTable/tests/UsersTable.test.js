import React from 'react';
import { usersResponseMock } from '../../../../__mocks__/usersResponseMock';
import { fireEvent, render, screen, within } from '../../../utils/test-wrapper';
import { UsersTable } from '../UsersTable';
import { usersTableConfig } from '../usersTableConfig';


const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useHistory: () => ({
        push: mockHistoryPush,
    }),
}));

beforeEach(() => {
    jest.resetAllMocks();
});

describe('UsersTable', () => {

    it('will render a Collapsible Table with as many header cells as userCells in config', () => {
        render(<UsersTable rows={[]} config={usersTableConfig} />);
        expect(screen.getByLabelText('collapsible table')).toBeInTheDocument();
        usersTableConfig.userCells.forEach(({ name }) => {
            const row = screen.getAllByRole('row')[0];
            const rowWithUtils = within(row);
            expect(rowWithUtils.getByText(name)).toBeInTheDocument();
        });
    });

    it('will render as many body rows as items in rows prop', () => {
        render(
            <UsersTable
                rows={usersResponseMock.content}
                config={usersTableConfig} />
        );

        const tableBody = screen.getAllByRole('rowgroup')[1];
        expect(tableBody.childNodes).toHaveLength(usersResponseMock.content.length * 2);
    });

    it('will render pagination details', () => {
        render(
            <UsersTable
                rows={usersResponseMock.content}
                config={usersTableConfig}
                rowsPerPage={2}
                count={3}
                page={1} />
        );
        const tableFoot = screen.getAllByRole('rowgroup')[2];
        expect(within(tableFoot).getByText('3-3 of 3')).toBeInTheDocument();
    });

    test('the onChangeRowsPerPage callback will push size and page to router hostory', () => {
        render(
            <UsersTable
                rows={usersResponseMock.content}
                config={usersTableConfig} />
        );
        const size = 2;
        const selector = screen.getByLabelText('rows per page');
        fireEvent.change(selector, { target: { value: size } });
        expect(mockHistoryPush).toHaveBeenCalledWith(`?size=${size}&page=1`);
    });

    test('the onChangePage callback will push the page number to router hostory', () => {
        render(
            <UsersTable
                rows={usersResponseMock.content}
                config={usersTableConfig}
                rowsPerPage={2}
                count={3}
                page={0} />
        );
        const nextPageButton = screen.getByLabelText('Next page');
        fireEvent.click(nextPageButton);
        expect(mockHistoryPush).toHaveBeenCalledWith('?page=2');
    });



});
