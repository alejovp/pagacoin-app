import React, { useCallback, useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import grey from '@material-ui/core/colors/grey';
import { DispatchContext } from '../../contexts/AppStore/AppStore';
import { setCurrentWallets, setShowTranferModal } from '../../contexts/AppStore/actions';
import { Wallet } from '../Wallet';


const useRowStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            borderBottom: 'unset',
        },
    },
    tableCell: {
        paddingBottom: 0,
        paddingTop: 0,
        backgroundColor: grey[400]
    },
    box: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    tableCellHide: {
        [theme.breakpoints.down('xs')]: {
            display: 'none',
        },
    }
}));

export const UsersTableRow = ({ row, config }) => {

    const [open, setOpen] = useState(false);
    const classes = useRowStyles();
    const dispatch = useContext(DispatchContext);

    const onExpandHandler = useCallback(
        () => setOpen(!open),
        [setOpen, open]
    );

    const onTransferHandler = useCallback((wallet) => {
        const restWallets = row.wallets.filter(w => w.walletId !== wallet.walletId);
        dispatch(setCurrentWallets(wallet, restWallets || []));
        dispatch(setShowTranferModal(true));
    }, [dispatch, row.wallets]);

    const renderArrowIcon = useCallback(() => {
        return open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />;
    }, [open]);

    const renderRowCells = (cellsConfig, row) => (
        cellsConfig.map((cell, i) => {
            let node = row[cell.param];
            if (cell.type === 'array') {
                node = node.length;
            }
            return (
                <TableCell key={i}
                           align={cell.align}
                           className={cell.hide && classes.tableCellHide}>
                    {node}
                </TableCell>
            );
        })
    );

    const renderBodyRows = () => {
        if (open) {
            return row.wallets.map((wallet, i) => (
                <Wallet key={i}
                        wallet={wallet}
                        onClick={onTransferHandler} />
            ));
        }
    };

    return (
        <>
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row"
                                size="small"
                                onClick={onExpandHandler}>
                        {renderArrowIcon()}
                    </IconButton>
                </TableCell>
                {renderRowCells(config.userCells, row)}
            </TableRow>
            <TableRow>
                <TableCell className={classes.tableCell}
                           colSpan={config.userCells.length + 1}>
                    <Collapse in={open}
                              timeout="auto"
                              unmountOnExit>
                        <Box className={classes.box}
                             margin={1}>
                            <Typography variant="subtitle2"
                                        gutterBottom
                                        component="div">
                                Wallets
                            </Typography>
                            {renderBodyRows()}
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
};

UsersTableRow.propTypes = {
    row: PropTypes.shape({
        userId: PropTypes.number.isRequired,
        userName: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        wallets: PropTypes.array.isRequired
    }).isRequired,
    config: PropTypes.object.isRequired
};
