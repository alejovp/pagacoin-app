import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { UsersTableRow } from './UsersTableRow';
import { useUrlSearchParams } from '../../hooks';


const useStyles = makeStyles(theme => ({
    paper: {
        minHeight: 300
    },
    tableCellHide: {
        [theme.breakpoints.down('xs')]: {
            display: 'none',
        },
    },
}));

export const UsersTable = ({
    rows, loading, config,
    rowsPerPage = 5, page = 0, count = 0
}) => {

    const history = useHistory();
    const searchUrl = useUrlSearchParams();
    const classes = useStyles();

    const onChangeRowsPerPage = useCallback(({ target }) => {
        const { value } = target;
        searchUrl.set('size', value);
        searchUrl.set('page', 1);
        history.push(`?${searchUrl}`);
    }, [searchUrl, history]);

    const onChangePage = useCallback((e, page) => {
        searchUrl.set('page', page + 1);
        history.push(`?${searchUrl}`);
    }, [searchUrl, history]);

    const renderTableHeaderCells = () => {
        if (config && config.userCells) {
            return config.userCells.map((cell, i) => (
                <TableCell align="center"
                           className={cell.hide && classes.tableCellHide}
                           key={i}>
                    {cell.name}
                </TableCell>
            ));
        }
    };

    const renderTableBody = () => {
        if (loading) {
            return null;
        }
        return rows.map((row) => (
            <UsersTableRow key={row.userId}
                           row={row}
                           config={config} />
        ));
    };

    return (
        <TableContainer component={Paper}
                        className={classes.paper}
                        data-testid="UsersTable">
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        {renderTableHeaderCells()}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {renderTableBody()}
                </TableBody>
                <TableFooter>
                    <TableRow>
                        <TablePagination
                            rowsPerPageOptions={[2, 5, 10, { label: 'All', value: -1 }]}
                            count={count}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            SelectProps={{
                                inputProps: { 'aria-label': 'rows per page' },
                                native: true,
                            }}
                            onChangePage={onChangePage}
                            onChangeRowsPerPage={onChangeRowsPerPage}
                        />
                    </TableRow>
                </TableFooter>
            </Table>
        </TableContainer>
    );
};

UsersTable.propTypes = {
    rows: PropTypes.array.isRequired,
    loading: PropTypes.bool,
    config: PropTypes.object.isRequired,
    rowsPerPage: PropTypes.number,
    page: PropTypes.number,
    count: PropTypes.number
};
