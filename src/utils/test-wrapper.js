import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter } from 'react-router-dom';
import { render } from '@testing-library/react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { AppStore } from '../contexts/AppStore/AppStore';
import { lightTheme } from '../styles/themes/lightTheme';


const AllTheProviders = ({ children }) => {
    return (
        <AppStore>
            <MuiThemeProvider theme={lightTheme}>
                <BrowserRouter>
                    {children}
                </BrowserRouter>
            </MuiThemeProvider>
        </AppStore>
    );
};

AllTheProviders.propTypes = {
    children: PropTypes.node
};

const customRender = (ui, { route = '/', ...rest } = {}) => {
    window.history.pushState({}, 'Test page', route);
    return render(ui, { wrapper: AllTheProviders, ...rest });
};


// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
