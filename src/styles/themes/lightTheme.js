import { createMuiTheme } from '@material-ui/core/styles';

export const lightTheme = createMuiTheme({
    constants: {
        drawerWidth: 180
    },
    breakpoints: {
        values: {
            xs: 0,
            sm: 670,
            md: 980,
            lg: 1280,
            xl: 1920,
        },
    },
    overrides: {
        MuiCssBaseline: {
            '@global': {
                'html, body': {
                    margin: 0,
                    padding: 0
                },
                '*, *::after, *::before': {
                    boxSizing: 'border-box'
                },
                '#root': {
                    display: 'flex'
                }
            },
        }
    },
});
