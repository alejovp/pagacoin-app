export const usersResponseMock = {
    content: [
        {
            userId: 1,
            userName: 'user1',
            firstName: 'Jhon',
            lastName: 'Snow',
            wallets: [
                {
                    walletId: 9,
                    hash: 'HASH9A',
                    balance: 100000000,
                    userId: 1
                }
            ],
            totalBalance: 23
        },
        {
            userId: 2,
            userName: 'user2',
            firstName: 'Walter',
            lastName: 'White',
            wallets: [
                {
                    walletId: 4,
                    hash: 'HASH4A',
                    balance: 1000,
                    userId: 2
                },
                {
                    walletId: 8,
                    hash: 'HASH8A',
                    balance: 10000000,
                    userId: 2
                }
            ],
            totalBalance: 2345
        },
        {
            userId: 3,
            userName: 'user3',
            firstName: 'Walter',
            lastName: 'White',
            wallets: [
                {
                    walletId: 4,
                    hash: 'HASH4A',
                    balance: 1000,
                    userId: 2
                },
                {
                    walletId: 8,
                    hash: 'HASH8A',
                    balance: 10000000,
                    userId: 2
                }
            ],
            totalBalance: 123
        }
    ],
    pageable: {
        sort: {
            sorted: false,
            unsorted: true,
            empty: true
        },
        offset: 0,
        pageNumber: 0,
        pageSize: 2,
        paged: true,
        unpaged: false
    },
    last: false,
    totalPages: 8,
    totalElements: 16,
    numberOfElements: 2,
    first: true,
    number: 0,
    sort: {
        sorted: false,
        unsorted: true,
        empty: true
    },
    size: 2,
    empty: false
};
