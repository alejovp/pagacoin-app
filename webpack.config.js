const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BUILD_GLOBALS = require('./scripts/globals');
const { ENV, APP_DIR, PUBLIC_DIR } = BUILD_GLOBALS;
const isLocalEnv = ENV === 'local';

module.exports = {
    mode: isLocalEnv ? 'development' : 'production',
    devtool: isLocalEnv ? 'source-map' : undefined,
    entry: {
        index: `${APP_DIR}/index.js`
    },
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        historyApiFallback: true,
        open: true,
        compress: true,
        port: 8081,
    },
    output: {
        filename: '[name].bundle.js',
        pathinfo: true,
        sourceMapFilename: '[name].bundlejs.map',
        path: PUBLIC_DIR,
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: [APP_DIR],
                loader: 'babel-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'PagaCoin BO',
            hash: true,
            inject: true,
            mobile: true,
            template: `${APP_DIR}/index.ejs`,
            filename: `${PUBLIC_DIR}/index.html`,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                preserveLineBreaks: false
            },
            favicons: []
        })
    ]
};
