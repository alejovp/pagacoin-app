# PagaCoin App #

This is a React dummy app created with [material-ui](https://material-ui.com), [axios](https://github.com/axios/axios), [jest](https://jestjs.io/docs/en/getting-started) and [react-testing-library](https://testing-library.com/docs/react-testing-library/intro).
Base project tooling includes node, webpack, babel and eslint.

## Setup:

Builded using node `v14.15.0`.

Clone this repo and at the root level run:
```
$ npm i
```

## Running the project:

In order to run the project just do:
```
$ npm run dev
```

This will create the app bundle, run eslint, a local server on port `8081` with hot reloading and open a browser window pointing to this server.

For unit testing:
```
$ npm run test
```
or for code coverage:
```
$ npm run test-coverage
```
